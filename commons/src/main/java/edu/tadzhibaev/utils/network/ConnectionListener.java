package edu.tadzhibaev.utils.network;

import java.io.IOException;

public interface ConnectionListener {

    void onMessageReceived(Connection connection, String message) throws IOException;

    void onDisconnected(Connection connection);

    void onException(Connection connection, Exception e);

}
