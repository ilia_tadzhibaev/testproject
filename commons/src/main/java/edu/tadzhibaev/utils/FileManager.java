package edu.tadzhibaev.utils;

import java.io.File;
import java.io.IOException;

public class FileManager {

    public static synchronized File createLogFileIfNotExists(String userLogin) throws IOException {
        File logFile = new File(Configuration.LOGS_DIR + File.separator + userLogin + ".txt");

        if (!logFile.exists()) {
            logFile.getParentFile().mkdirs();
            logFile.createNewFile();
            System.out.println("file created: " + logFile.toPath());
        } else {
            System.out.println("file exists: " + logFile.toPath());
        }
        return logFile;
    }
}
