package edu.tadzhibaev.utils.loggers.impl;

import edu.tadzhibaev.utils.loggers.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileLogger implements Logger {

    private FileWriter writer;
    private File logFile;

    public FileLogger(File logFile) throws IOException {
        this.logFile = logFile;
        writer = new FileWriter(logFile, true);
    }

    @Override
    public void logMessage(String message) throws IOException {
        writer.write(message + "\r\n");
        writer.flush();
    }

    @Override
    public void close() throws IOException {
        System.out.println("Closing fileLogger");
        writer.close();
    }

    public File getLogFile() {
        return logFile;
    }
}
