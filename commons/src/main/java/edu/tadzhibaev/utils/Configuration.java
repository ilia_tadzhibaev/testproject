package edu.tadzhibaev.utils;

import java.io.IOException;
import java.util.Properties;

public class Configuration {

    public final static int SERVER_PORT = Integer.parseInt(loadProperties().getProperty("server.port"));
    public final static String SERVER_HOST = loadProperties().getProperty("server.host");
    public final static String LOGS_DIR = loadProperties().getProperty("logs.dir");

    private Configuration() {
    }

    private static Properties loadProperties() {
        try {
            Instance.PROPERTIES.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
        } catch (IOException e) {
            throw new RuntimeException("Unable to load properties");
        }
        return Instance.PROPERTIES;
    }

    private static class Instance {
        private static final Properties PROPERTIES = new Properties();
    }


}
