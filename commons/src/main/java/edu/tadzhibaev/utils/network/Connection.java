package edu.tadzhibaev.utils.network;

import java.io.*;
import java.net.Socket;

public class Connection {

    private final ConnectionListener connectionListener;
    private final Socket socket;
    private BufferedWriter writer;
    private BufferedReader reader;
    private Thread thread;

    public Connection(ConnectionListener connectionListener, String host, Integer port) throws IOException {
        this(connectionListener, new Socket(host, port));
    }

    public Connection(ConnectionListener connectionListener, Socket socket) {
        this.connectionListener = connectionListener;
        this.socket = socket;

        try {
            writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            connectionListener.onException(this, e);
        }
    }

    public Connection runNewThread() {
        thread = new Thread(() -> {
            try {
                System.out.println("buffered reader initialized");
                while (!thread.isInterrupted()) {
                    String message = reader.readLine();
                    connectionListener.onMessageReceived(Connection.this, message);
                }

            } catch (IOException e) {
                connectionListener.onException(this, e);
            }
        });

        thread.start();
        return this;
    }

    public void sendMessage(String message) {
        try {
            writer.write(message + "\r\n");
            writer.flush();
        } catch (IOException e) {
            connectionListener.onException(this, e);
        }
    }

    public void disconnect() {
        try {
            thread.interrupt();
            socket.close();
        } catch (IOException e) {
            connectionListener.onException(this, e);
        }
    }
}
