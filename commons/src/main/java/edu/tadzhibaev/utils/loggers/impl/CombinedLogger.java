package edu.tadzhibaev.utils.loggers.impl;

public class CombinedLogger {

    private ConsoleLogger consoleLogger;
    private FileLogger fileLogger;

    public CombinedLogger(ConsoleLogger consoleLogger, FileLogger fileLogger) {
        this.consoleLogger = consoleLogger;
        this.fileLogger = fileLogger;
    }
}
