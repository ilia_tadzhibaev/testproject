package edu.tadzhibaev.utils.calculator;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.script.ScriptException;

import static org.testng.Assert.assertEquals;

public class CalculatorTest {

    @DataProvider
    public Object[][] positiveCases() {
        return new Object[][] {
                {"0+19", 19},
                {"0-19", -19},
                {"(5+6)*(2+3)", 55},
                {"(5+6)*(2-3)", -11},
                {"5+6+7-0", 18},
        };
    }

    @Test(dataProvider = "positiveCases")
    public void testCalculate(String expression, Integer expected) throws ScriptException, WrongFormatException {
        assertEquals(Calculator.calculate(expression), expected);
    }

    @DataProvider
    public Object[][] wrongFormatExceptionTest() {
        return new Object[][] {
                {"7^9"},
                {"6/8)"},
                {"5%9"},
                {"adfadfa"},
        };
    }

    @Test(expectedExceptions =  WrongFormatException.class,
            dataProvider = "wrongFormatExceptionTest")
    public void wrongFormatException(String expression) throws ScriptException, WrongFormatException {
        Calculator.calculate(expression);
    }

    @DataProvider
    public Object[][] scriptExceptionTest() {
        return new Object[][] {
                {"(("},
                {"7++9"},
                {"-4-("},
                {"6+7)"},
                {"((6+7)"},
        };
    }

    @Test(expectedExceptions =  ScriptException.class,
            dataProvider = "scriptExceptionTest")
    public void parsingException(String expression) throws ScriptException, WrongFormatException {
        Calculator.calculate(expression);
    }
}