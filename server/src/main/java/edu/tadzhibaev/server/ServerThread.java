package edu.tadzhibaev.server;

import edu.tadzhibaev.utils.FileManager;
import edu.tadzhibaev.utils.calculator.Calculator;
import edu.tadzhibaev.utils.calculator.WrongFormatException;
import edu.tadzhibaev.utils.loggers.impl.ConsoleLogger;
import edu.tadzhibaev.utils.loggers.impl.FileLogger;
import edu.tadzhibaev.utils.network.Connection;
import edu.tadzhibaev.utils.network.ConnectionListener;

import javax.script.ScriptException;
import java.io.IOException;
import java.net.Socket;
import java.nio.file.Files;
import java.util.Collections;
import java.util.List;

public class ServerThread implements ConnectionListener {

    private final ConsoleLogger consoleLogger;
    private final Socket socket;
    private final Connection connection;
    private FileLogger fileLogger;
    private String userLogin;
    private int messagesCounter = 0;

    public ServerThread(Socket socket) {
        this.socket = socket;
        this.connection = new Connection(this, socket).runNewThread();

        consoleLogger = new ConsoleLogger();
        consoleLogger.logMessage("Connection established.");
        connection.sendMessage("Enter your name:");
    }

    @Override
    public void onMessageReceived(Connection connection, String message) throws IOException {
        if (0 == messagesCounter++) {
            fileLogger = new FileLogger(FileManager.createLogFileIfNotExists(message));
            initConnection(connection, message);
            return;
        }

        if ("/history".equals(message)) {
            sendLogHistory(getLogHistory());
        } else {
            Integer result;
            try {
                result = Calculator.calculate(message);
            } catch (WrongFormatException | ScriptException e) {
                connection.sendMessage(e.getMessage());
                return;
            }

            connection.sendMessage(String.format("Expression result: %d", result));
            fileLogger.logMessage(message);
        }
    }

    private void initConnection(Connection connection, String message) {
        userLogin = message;
        consoleLogger.logMessage(String.format("Logged user: %s%n", message));
        connection.sendMessage(String.format("Logged in as user '%s'. Please, enter a math expression(only +,-,* operations are supported. To view you log history, type /history", userLogin));
    }

    private void sendLogHistory(List<String> logLines) {
        connection.sendMessage("Your history(the latest is on top):");
        Collections.reverse(logLines);
        logLines.forEach(connection::sendMessage);
    }

    private List<String> getLogHistory() {
        try {
            return Files.readAllLines(fileLogger.getLogFile().toPath());
        } catch (IOException e) {
            throw new RuntimeException("Can't read file.");
        }
    }

    @Override
    public void onDisconnected(Connection connection) {
        consoleLogger.logMessage(String.format("Client '%s' disconnecting...", userLogin));
        try {
            socket.close();
            fileLogger.close();
        } catch (IOException e) {
            consoleLogger.logMessage(e.getMessage());
        }
        consoleLogger.logMessage(String.format("Client '%s' disconnected", userLogin));
    }

    @Override
    public void onException(Connection connection, Exception e) {
        consoleLogger.logMessage(String.format("Exception occurred: %s. Disconnecting '%s' client", e.getMessage(), userLogin));
        onDisconnected(connection);
    }
}
