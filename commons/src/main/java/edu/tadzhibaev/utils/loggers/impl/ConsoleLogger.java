package edu.tadzhibaev.utils.loggers.impl;

import edu.tadzhibaev.utils.loggers.Logger;

import java.io.Console;

public class ConsoleLogger implements Logger {

    @Override
    public void logMessage(String message) {
        System.out.println(message);
    }

    @Override
    public void close() {
        System.out.println("Console logger terminated");
    }
}
