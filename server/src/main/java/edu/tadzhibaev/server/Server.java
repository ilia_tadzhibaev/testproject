package edu.tadzhibaev.server;

import edu.tadzhibaev.utils.Configuration;

import java.io.IOException;
import java.net.ServerSocket;

public class Server {

    Server() throws IOException {
        try (ServerSocket serverSocket = new ServerSocket(Configuration.SERVER_PORT)) {
            while (true) {
                new ServerThread(serverSocket.accept());
            }
        }
    }

}
