package edu.tadzhibaev.utils.calculator;

import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Calculator {

    private Calculator() {
    }

    private static void validateExpression(String expression) throws WrongFormatException {
        if (!expression.matches("(\\(?[*\\-+]?\\d?\\)?[*\\-+]*?\\)?)*")) {
            throw new WrongFormatException("Expression format is incorrect. Try again");
        }
    }

    public static Integer calculate(String expression) throws WrongFormatException, ScriptException {
        validateExpression(expression);
        expression = expression.replaceAll(" ", "");

        return (Integer) new ScriptEngineManager().getEngineByName("JavaScript").eval(expression);
    }

}
