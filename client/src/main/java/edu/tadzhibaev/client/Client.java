package edu.tadzhibaev.client;

import edu.tadzhibaev.utils.Configuration;
import edu.tadzhibaev.utils.loggers.impl.ConsoleLogger;
import edu.tadzhibaev.utils.network.Connection;
import edu.tadzhibaev.utils.network.ConnectionListener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Client implements ConnectionListener {

    private Connection connection;
    private BufferedReader bufferedReader;
    private ConsoleLogger consoleLogger;

    Client() throws IOException {
        this.connection = new Connection(this, Configuration.SERVER_HOST, Configuration.SERVER_PORT).runNewThread();
        this.bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        this.consoleLogger = new ConsoleLogger();
    }

    void runListeningToLines() throws IOException {
        while (true) {
            if (bufferedReader.ready()) {
                String clientMessage = bufferedReader.readLine();

                if (!"quit".equalsIgnoreCase(clientMessage)) {
                    connection.sendMessage(clientMessage);
                } else {
                    onDisconnected(connection);
                }
            }
        }
    }

    @Override
    public void onMessageReceived(Connection connection, String message) {
        System.out.println(message);
    }

    @Override
    public void onDisconnected(Connection connection) {
        consoleLogger.logMessage("Closing connection");
        connection.disconnect();
        System.exit(-1);
    }

    @Override
    public void onException(Connection connection, Exception e) {
        consoleLogger.logMessage("Error occurred. Disconnection");
        onDisconnected(connection);
    }
}
