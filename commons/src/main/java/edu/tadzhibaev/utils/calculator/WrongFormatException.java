package edu.tadzhibaev.utils.calculator;

public class WrongFormatException extends Exception {

    public WrongFormatException(String message) {
        super(message);
    }

    public WrongFormatException(String message, Throwable cause) {
        super(message, cause);
    }
}
