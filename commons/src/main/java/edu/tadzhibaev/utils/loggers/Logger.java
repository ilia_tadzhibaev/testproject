package edu.tadzhibaev.utils.loggers;

import java.io.IOException;

public interface Logger {

    void logMessage(String message) throws IOException;

    void close() throws IOException;

}
